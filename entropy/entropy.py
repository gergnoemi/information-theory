# Gergely Noémi-Laura
# gnim1720

import glob, re, math
import errno
from collections import Counter

def read_from_files():
    #change to corresponding path
    path = 'D:\Uni\InfTheory\Labor 1\*.txt'
    files = glob.glob(path)
    text = ''
    for name in files:
        try:
            with open(name, encoding='utf8') as f:
                text += f.read().lower()
        except IOError as exc:
            if exc.errno != errno.EISDIR:
                raise
            text += '\n'
    return text

def get_monograms(text):
    wordList = re.sub('[^\w]', ' ',  text).split()
    return Counter(wordList)

def get_bigrams(text):
    wordList = re.sub('[^\w]', ' ',  text).split()
    bigramList = []
    for i, j in zip(wordList, wordList[1:]):
        bigramList.append(i + ' ' + j)
    return Counter(bigramList)

def get_trigrams(text):
    wordList = re.sub('[^\w]', ' ',  text).split()
    trigramList = []
    for i, j, k in zip(wordList, wordList[1:], wordList[2:]):
        trigramList.append(i + ' ' + j + ' ' + k)
    return Counter(trigramList)

def calc_entropy(counter, t):
    sum = 0
    instances = 0
    for c in counter:
        instances += counter[c]
        sum += math.log(1/counter[c],2) * counter[c]
    if (t == 1):
        return (sum/instances + math.log(instances,2))
    elif (t == 2):
        return ((sum/instances + math.log(instances,2)) / 2)
    else:
        return ((sum/instances + math.log(instances,2)) / 3)

def main():
    text = read_from_files()

    monograms = get_monograms(text)
    print('Entropy of unigrams: ', calc_entropy(monograms,1))

    bigrams = get_bigrams(text)
    print('Entropy of bigrams: ', calc_entropy(bigrams,2))

    trigrams = get_trigrams(text)
    print('Entropy of trigrams: ', calc_entropy(trigrams,3))

if __name__ == '__main__':
    main()