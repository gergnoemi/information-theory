# Gergely Noémi-Laura
# gnim1720
# 531.

import sys, errno, numpy

def read_from_file(filename):
    with open(filename, 'r') as f:
        text = f.read()

    return text

def read_lz77_from_file(filename):
    codes = []
    with open(filename, 'r') as f:
        for l in f.readlines():
            l = l.rstrip('\n')
            tmp = l.split(' ')
            codes.append((int(tmp[0]), int(tmp[1]), tmp[2]))

    return codes

def read_lz78_from_file(filename):
    codes = []
    with open(filename, 'r') as f:
        for l in f.readlines():
            l = l.rstrip('\n')
            tmp = l.split(' ')
            codes.append((int(tmp[0]), tmp[1]))

    return codes

def read_lzw_from_file(filename):
    codes = []
    alphabet = []
    with open(filename, 'r') as f:
       line = f.readline()
       codes = line.rstrip('\n').split(' ')
       line = f.readline()
       alphabet = line.rstrip('\n').split(' ')
    
    return list(map(int, codes)), alphabet

def write_lz77_to_file(codes, filename):
    with open(filename, 'w') as f:
        f.write('\n'.join('{} {} {}'.format(code[0], code[1], code[2]) for code in codes))

def write_lz78_to_file(codes, filename):
    with open(filename, 'w') as f:
        f.write('\n'.join('{} {}'.format(code[0], code[1]) for code in codes))

def write_lzw_to_file(codes, alphabet, filename):
    with open(filename, 'w') as f:
        f.write(' '.join((str(code) for code in codes)))
        f.write('\n')
        f.write(' '.join((str(char) for char in alphabet)))

# no look-ahead window implemented
def compress_with_lz77(text, search_size):
    codes = []
    search_bf = ''
    word = ''

    for char in text:
        word += char

        if word not in search_bf:

            if (len(word) > 1):
                offset = len(search_bf) - search_bf.find(word[:-1])
            else:
                offset = 0

            if offset:
                match = len(word)-1
            else:
                match = 0

            codes.append((offset, match, word[-1]))

            search_bf += word
            word = ''

        # update search buffer size and content
        if (len(search_bf) >= search_size):
            search_bf = search_bf[-search_size:]

    # the last word has a match in search buffer
    if word:
        if (len(word) > 1):
            offset = len(search_bf) - search_bf.find(word[:-1])
            match = len(word)
        else:
            offset = len(search_bf) - search_bf.find(word)
            match = 1

        codes.append((offset, match, '-'))

    # print(codes)

    return codes

def compress_with_lz78(text):
    dictionary = {}
    size = 0
    word = ''

    codes = []
    index = 0
    for char in text:
        word += char
        if word in dictionary:
            if len(word) > 1:
                index = dictionary[word]
            else:
                index = dictionary[char]

        else:
            size += 1
            dictionary[word] = size
            word = ''
            codes.append((index, char))
            index = 0

    if (word != ''):
        codes.append((index, ''))

    return codes

def compress_with_lzw(text):
    dictionary = {}
    dict_size = 0

    alphabet = set(text)
    for char in alphabet:
        dict_size += 1
        dictionary[char] = dict_size
 
    codes = []
    
    word = ''
    for char in text:
        code = word + char
        if code in dictionary:
            word = code
        else:
            codes.append(dictionary[word])
            dict_size += 1
            dictionary[code] = dict_size
            word = char
 
    if word:
        codes.append(dictionary[word])

    return codes, alphabet

def decompress_lz77(codes): 
    text = ''
    for code in codes:
        if (code[0] != 0):
            word = text[-code[0]:][:code[1]]
            text += word
        if (code[2] == '-'):
            return text
        text += code[2]

    return text

def decompress_lz78(codes):
    dictionary = {}
    size = 0
    text = ''

    for code in codes:
        size += 1
        if (code[0] == 0):
            dictionary[size] = code[1]
            text += code[1]
        else:
            symbol = dictionary[code[0]] + code[1]
            dictionary[size] = symbol
            text += symbol

    return text

def decompress_lzw(codes, alphabet):
    dictionary = {}
    dict_size = 0

    for char in alphabet:
        dict_size += 1
        dictionary[dict_size] = char

    word = dictionary[codes.pop(0)]
    text = word
    
    for code in codes:
        if code in dictionary:
            wc = dictionary[code]
        else:
            wc = word + word[0]
        text += wc

        dict_size += 1
        dictionary[dict_size] = word + wc[0]
        
        word = wc

    return text

def main():
    if (len(sys.argv) != 2):
        errno('Expected argument missing: input file name')
        exit

    text = read_from_file(sys.argv[1])

    #Compress with LZ77 and save to a file
    write_lz77_to_file(compress_with_lz77(text, 8),'compressed_lz77.txt')
    #Read from file and decompress LZ77
    codes = read_lz77_from_file('compressed_lz77.txt')
    print(decompress_lz77(codes))

    #Compress with LZW
    codes, alphabet = compress_with_lzw(text)
    #Save to a file
    write_lzw_to_file(codes, alphabet, 'compressed_lzw.txt')
    #Read from a file
    codes, alphabet = read_lzw_from_file('compressed_lzw.txt')
    #Decompress LZW
    print(decompress_lzw(codes, alphabet))

    # write_lz78_to_file(codes, 'compressed.txt')
    # print(decompress_lz78(read_lz78_from_file('compressed.txt')))

if __name__ == '__main__':
    main()