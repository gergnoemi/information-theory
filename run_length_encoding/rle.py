# Gergely Noémi-Laura
# gnim1720

from PIL import Image
import glob, sys, numpy, errno
from collections import Counter

# Open .BMP image from path
def open_image(path):
  image = Image.open(path)
  return image

# Save Image object in .bmp format
def save_image(image, path):
    image.save(path+'.bmp')

# Compress RLE'ed image to binary format
def compress_image_to_binary(arr, name):
    numpy.save(name, arr)

# Uncompress binary file to RLE values
def uncompress_image(path):
    return numpy.load(path, allow_pickle=True)

# Create Image object with given resolutions
def create_image(i, j):
  image = Image.new("RGB", (i, j), "white")
  return image

# RLE an Image object
def encode_image(image):
    w, h = image.size
    values = [[w,h]]
    
    for i in range(0,h):
        row_values = []
        prev_p = -1
        count = 1
        for j in range(0,w):
            p = image.getpixel((j,i))
            if p != prev_p:
                if prev_p != -1:
                    row_values.append((prev_p, count))
                count = 1
                prev_p = p
            else:
                count += 1
        row_values.append((prev_p, count))
        values.append(row_values)    

    return values

# Decode a RLE'ed image to Image object
def decode_image(values):
    w, h = values[0]
    image = create_image(w,h)
    pixels = image.load()

    for i in range(1, len(values)):
        row = values[i]
        pos = 0
        for elem in row:
            pixel = elem[0]
            count = elem[1]
            for j in range(count):
                pixels[pos+j, i-1] = (int(pixel[0]), int(pixel[1]), int(pixel[2]))
            pos += count

    return image

def main():
    if (len(sys.argv) != 2):
        errno('Expected argument missing: .BMP file name')
        exit

    image = open_image(sys.argv[1])
    # RLE's and compresses image
    compress_image_to_binary(encode_image(image), 'encoded_image')

    # Decodes image
    # Saves decoded image as .BMP file
    values = uncompress_image('encoded_image.npy')
    image = decode_image(values)
    save_image(image,'decoded_image')

if __name__ == '__main__':
    main()